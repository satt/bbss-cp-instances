bench2:
- Used in publications for EvoCOP, HM, JOGO, Eurocast
- Created using the travel time file traveltime_millis_school_0800.csv

bench3:
- Multiple travel time matrices
- Travel time files: traveltime_millis_school_0430.csv, traveltime_millis_school_0800.csv, traveltime_millis_school_1200.csv, traveltime_millis_school_1815.csv
