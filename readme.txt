File names
===========
[Type]_[|Stations|]_[|Depots|]_[Counter].bbs

Type is either B (perfect balance can be achieved) or U (perfect balance cannot be achieved)

e.g. B_020_1_03.bbs: Perfect balance can be achieved, 20 stations, 1 depot, instance 3


Instance format
==============
Line 1: [|Stations|] [|Depots|]

Lines for stations: [Capacity] [Current number of bikes] [Target number of bikes] [Additive weight when station is NOT visited*]

Matrix of traveling times:
(Note that times are expected to already include an average time needed for parking at the station and for loading/unloading bikes. The matrices are also expected to be sorted by increasing time of day. If only one matrix is given the starting time must be omitted!)
[Number of travel time matrices] [Start time of 1st matrix (in minutes from midnight)] [Start time of 2nd matrix] ...
[Station 0] [Station 1] ... [Station |stations|-1] [Depot 0] ... [Depot |depots|-1]
...
[Station 0] [Station 1] ... [Station |stations|-1] [Depot 0] ... [Depot |depots|-1]
... (possible further travel time matrices)

Matrix of demand values (for dynamic rebalancing):
NOTE: We have two matrices - for bike demands and the other one for slot demands, so the following lines are 2 * |T|
[Number of intervals for which demand values are given (i.e. |T|)]
[Time T_1] [Bike Demand at station 0] [Bike Demand at station 1] ... [Bike Demand at station |stations|-1]
...
[Time T_{T^}] [Bike Demand at station 0] [Bike Demand at station 1] ... [Bike Demand at station |stations|-1]

[Time T_1] [Slot Demand at station 0] [Slot Demand at station 1] ... [Slot Demand at station |stations|-1]
...
[Time T_{T^}] [Slot Demand at station 0] [Slot Demand at station 1] ... [Slot Demand at station |stations|-1]

Citybike internal station IDs: [Station 0] ... [Station |stations|-1] [Depot 0] ... [Depot |depots|-1]

Matrix for e^beta values (multiplicative factor if the station gets FULL):
[Number of nearest neighborhood stations to be considered (i.e. |N|)]
[No Boxes at station 0 N_0] [No Boxes at station 0 N_1] ... [No Boxes at station 0 N_{N^}]
...
[No Boxes at station |stations|-1 N_0] [No Boxes at station |stations|-1 N_1] ... [No Boxes at station |stations|-1 N_{N^}]

[No Bikes at station 0 N_0] [No Bikes at station 0 N_1] ... [No Bikes at station 0 N_{N^}]
...
[No Bikes at station |stations|-1 N_0] [No Bikes at station |stations|-1 N_1] ... [No Bikes at station |stations|-1 N_{N^}]





All times should be given in minutes.

* This weight represents a penalty that is added to the objective value if this station is not visited at least once by any vehicle. The value should be >0 for stations that should be visited with higher priority, e.g., because defective bikes need to be picked up.
