Vehicle instance format
==============

Line 1: [|Vehicles|]
	
Lines for vehicles: [Capacity] [Number of bikes on vehicle] [Remaining shift length] [Start node] [End node]


All times should be given in minutes.
